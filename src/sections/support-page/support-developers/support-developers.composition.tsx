import React from 'react';
import { ThemeCompositions } from '@teambit/documenter.theme.theme-compositions';
import { SupportDevelopers } from './support-developers';

export const SupportDevelopersExample = () => (
	<ThemeCompositions>
		<SupportDevelopers data-testid="test-support" />
	</ThemeCompositions>
);
