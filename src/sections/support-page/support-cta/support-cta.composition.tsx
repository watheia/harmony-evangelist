import React from 'react';
import { ThemeCompositions } from '@teambit/documenter.theme.theme-compositions';
import { SupportCta } from './support-cta';

export const SupportCtaExample = () => (
	<ThemeCompositions>
		<SupportCta data-testid="test-support" />
	</ThemeCompositions>
);
